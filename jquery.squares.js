/*global setInterval, clearInterval, setTimeout, window, jQuery, console, document*/

(function ($, Modernizr, console) {

    "use strict";

    var animationDuration = 500,
        useImgTag = !Modernizr.backgroundsize,
        useTransitions = Modernizr.csstransitions,
        useTransforms = Modernizr.csstransforms,

        // events
        prepareEvent = 0,
        startEvent = 1,
        stepEvent = 2,
        endEvent = 3,

        AnimationHelper,
        Tile,
        TiledAnimationHelper,
        TiledBackground,

        methods;

    console.log('transition support: ' + Modernizr.csstransitions);
    console.log('transform support: ' + Modernizr.csstransforms);
    console.log('backgroundSize support: ' + Modernizr.backgroundsize);


    AnimationHelper = function (tile) {

        var that = this,
            transitionEvents = 'transitionend webkitTransitionEnd';
        this.deferred = $.Deferred();

        tile.$element.on(transitionEvents, function applyCallback() {
            that.deferred.notify();
        });

        this.tile = tile;
        this.$element = tile.$element;
    };
    AnimationHelper.prototype.promise = function (tile) {
        this.deferred.promise(tile);
    };
    AnimationHelper.prototype.setPosition = function (position) {
        if (useTransforms) {
            this.$element.css('transform', 'translate(' + position.x + 'px, ' +
                position.y + 'px) rotate(' + position.rotation + 'deg)');
        } else {
            this.$element.css('left',  position.x + 'px');
            this.$element.css('top',  position.y + 'px');
        }
    };
    AnimationHelper.prototype.staticPosition = function (to) {
        var that = this;
        this.$element.animate({
            height: this.tile.size,
            width: this.tile.size
        }, animationDuration, function () {
            that.deferred.notify();
        });
    };
    AnimationHelper.prototype.animatePosition = function (from, to) {

        if (useTransitions) {

            this.setPosition(to);

        } else {

            var that = this,
                interval,
                frames,
                currentFrame,
                deltaX,
                deltaY,
                deltaRotation,
                int;

            if (useTransforms) {

                interval = 20;
                frames = animationDuration / interval;
                currentFrame = 0;
                deltaX = (to.x - from.x) / frames;
                deltaY = (to.y - from.y) / frames;
                deltaRotation = 720 / frames;

                int = setInterval(function animationStep() {

                    currentFrame += 1;
                    from.x += deltaX;
                    from.y += deltaY;
                    from.rotation += deltaRotation;

                    that.setPosition(from);

                    if (currentFrame >= frames) {
                        that.deferred.notify();
                        clearInterval(int);
                    }
                }, interval);
            } else {
                this.$element.animate({
                    top: to.y,
                    left: to.x
                }, animationDuration, function () {
                    that.deferred.notify();
                });
            }
        }
    };


    Tile = function (config) {

        var tileOffset,
            imgOffset,
            getHTML;

        tileOffset = {
            top: config.size * config.row,
            left: config.size * config.column
        };
        imgOffset = {
            top: -tileOffset.top + config.offset.top,
            left: -tileOffset.left + config.offset.left
        };

        getHTML = function () {
            var html = [];

            if (useImgTag) {
                html.push('<div class="tile" ');
                html.push('style="display: none;width:' + config.size + 'px;');
                html.push('height:' + config.size + 'px;">');
                html.push('<img width="' + config.imgWidth + '" height="' +
                    config.imgHeight + '" ');
                html.push('style="width:' + config.imgWidth + 'px; height:' +
                    config.imgHeight + 'px;left: ' + imgOffset.left
                    + 'px; top: ' + imgOffset.top + 'px;">');
                html.push('</div>');
            } else {
                html.push('<div class="tile" ');
                html.push('style="display: none;width:' + (config.size) + 'px;');
                html.push('height:' + (config.size) + 'px;');
                html.push('background-size:' + config.imgWidth + 'px ' +
                    config.imgHeight + 'px;');
                html.push('background-position: ' + imgOffset.left + 'px ' +
                    imgOffset.top + 'px;');
                html.push('"></div>');
            }
            return html.join('');
        };

        this.isStatic = config.isStatic;
        this.row = config.row;
        this.column = config.column;
        this.$element = $(getHTML());
        this.to = {
            x: tileOffset.left,
            y: tileOffset.top,
            rotation: 720
        };

        this.helper = new AnimationHelper(this);
        this.helper.promise(this);

        this.size = config.size;

        if (useImgTag) {
            this.$img = $('img', this.$element);
        }
    };
    Tile.prototype.setBackground = function (background) {
        if (useImgTag) {
            this.$img.attr('src', background);
        } else {
            this.$element.css('background-image', 'url(' + background + ')');
        }
    };
    Tile.prototype.prepare = function (row, column) {
        if (!this.isStatic) {
            this.moveTo(row, column);
        } else {
            this.helper.setPosition(this.to);
            this.$element.height(0);
            this.$element.width(0);
        }
    };
    Tile.prototype.moveTo = function (row, column) {

        var animateClass = 'animate';
        if (!this.isStatic) {
            this.$element.removeClass(animateClass);
        }

        this.from = {
            x: column * this.size,
            y: row * this.size,
            rotation: 0
        };
        this.helper.setPosition(this.from);

        if (!this.isStatic) {
            this.$element.addClass(animateClass);
        }
    };
    Tile.prototype.animate = function () {
        if (!this.isStatic) {
            this.helper.animatePosition(this.from, this.to);
        } else {
            this.helper.staticPosition(this.to);
        }
    };


    TiledAnimationHelper = function () {

        this.deferred = $.Deferred();

        this.animations = {
            'columns': this.columnsAnimation,
            'delay': this.delayAnimation,
            'linear': this.linearAnimation,
            'test': this.testAnimation,
            'snake': this.snakeAnimation
        };

    };
    TiledAnimationHelper.prototype.promise = function (tiled) {
        this.deferred.promise(tiled);
    };
    TiledAnimationHelper.prototype.setTiles = function (tiles, columns) {

        var that = this,
            counter = 0,
            onAnimationProgress,
            i;

        this.tiles = tiles;
        this.columns = columns;
        this.nColumns = columns.length;
        this.nRows = columns[0].length;

        // callback called when a single animation ends
        onAnimationProgress = function () {

            counter += 1;

            if (counter === that.nAnimations) {
                counter = 0;
                //notify animation's end
                that.deferred.notifyWith(that, [endEvent]);
            }
        };
        for (i = 0; i < this.tiles.length; i += 1) {
            this.tiles[i].progress(onAnimationProgress);
        }
    };
    TiledAnimationHelper.prototype.testAnimation = function () {

        var tile = this.tiles[0],
            deferred = this.deferred;

        this.nAnimations = 1;
        tile.$element.show(0, function () {
            deferred.notify('startStep');
            tile.animate();
        });
    };
    TiledAnimationHelper.prototype.animateDelayed = function (tile, delay, next) {

        var that = this,
            nextTile = next(tile),
            i,
            animateTile = function (tile) {
                tile.$element.show(0, function () {
                    tile.animate();
                });
            };

        if ($.isArray(tile)) {
            for (i = 0; i < tile.length; i += 1) {
                animateTile(tile[i]);
            }
        } else {
            animateTile(tile);
        }

        if (nextTile) {
            setTimeout(function animateNext() {
                that.animateDelayed(nextTile, delay, next);
            }, delay);
        }
    };
    TiledAnimationHelper.prototype.columnsAnimation = function (row, column, delay) {

        var inverse = column < this.columns.length / 2,
            startColumn = inverse ? this.columns.length - 1 : 0,
            that = this,

            // animate one column at a time
            next = function (tile) {

                var nextColumn = tile[0].column,
                    isOut;

                nextColumn = inverse ? nextColumn - 1 : nextColumn + 1;
                isOut = inverse ? nextColumn < 0 : nextColumn >= that.columns.length;
                return isOut ? null : that.columns[nextColumn];
            };

        this.nAnimations = this.tiles.length;
        this.animateDelayed(this.columns[startColumn], delay, next);
    };
    TiledAnimationHelper.prototype.delayAnimation = function (row, column, delay) {
        var that = this,
            fromBottom = row < this.nRows / 2,
            fromRight = column < this.nColumns / 2,
            start = {
                row: fromBottom ? this.nRows - 1 : 0,
                column: fromRight ? this.nColumns - 1 : 0
            },
            startTile = this.columns[start.column][start.row],

            next = function (tile) {
                var nextColumn = tile.column,
                    nextRow = tile.row,
                    lastInRow = (fromBottom && nextRow === 0) ||
                        (!fromBottom && nextRow === that.nRows - 1),
                    isOut;

                if (lastInRow) {
                    nextRow = fromBottom ? that.nRows - 1 : 0;
                    nextColumn = fromRight ? nextColumn - 1 : nextColumn + 1;
                } else {
                    nextRow = fromBottom ? nextRow - 1 : nextRow + 1;
                }

                isOut = nextColumn < 0 || nextColumn >= that.nColumns ||
                    nextRow < 0 || nextRow > that.nRows;
                return isOut ? null : that.columns[nextColumn][nextRow];
            };

        this.nAnimations = this.tiles.length;
        this.animateDelayed(startTile, delay, next);
    };
    TiledAnimationHelper.prototype.snakeAnimation = function (row, column, delay) {
        var that = this,
            fromBottom = row < this.nRows / 2,
            fromRight = column < this.nColumns / 2,
            start = {
                row: fromBottom ? this.nRows - 1 : 0,
                column: fromRight ? this.nColumns - 1 : 0
            },
            startTile = this.columns[start.column][start.row],
            isOut,

            next = function (tile) {
                var nextColumn = tile.column,
                    nextRow = tile.row,
                    lastInRow = (fromBottom && nextRow === 0) ||
                        (!fromBottom && nextRow === that.nRows - 1);

                if (lastInRow) {
                    fromBottom = fromBottom ? false : true;
                    nextColumn = fromRight ? nextColumn - 1 : nextColumn + 1;
                } else {
                    nextRow = fromBottom ? nextRow - 1 : nextRow + 1;
                }

                isOut = nextColumn < 0 || nextColumn >= that.nColumns ||
                    nextRow < 0 || nextRow > that.nRows;
                return isOut ? null : that.columns[nextColumn][nextRow];
            };

        this.nAnimations = this.tiles.length;
        this.animateDelayed(startTile, delay, next);

    };
    TiledAnimationHelper.prototype.animate = function (x, y, animation, delay) {

        if (!this.animating) {

            var row = Math.floor(y / 150),
                column = Math.floor(x / 150),
                i,
                tile,
                that = this,
                end = function () {
                    that.deferred.notify(endEvent);
                };

            // move the tiles to the origin of the animation
            for (i = 0; i < this.tiles.length; i += 1) {
                tile = this.tiles[i];
                tile.prepare(row, column);
            }
            this.deferred.notifyWith(this, // notify prepare
                [prepareEvent, {row: row, column: column}]);


            // start the animation
            setTimeout(function launchAnimation() {
                that.deferred.notifyWith(that, [startEvent]); // notify start
                if (typeof animation === 'function') {
                    animation.apply(that, [that.tiles, row, column, delay, end]);
                } else {
                    that.animations[animation].apply(that, [row, column, delay]);
                }
            }, 0);
        }
    };


    TiledBackground = function (options) {

        var settings = $.extend({
            $target: null,       // required
            imageRatio: null,    // required (width / height)
            tileSize: 150,
            delay: 50,
            isStatic: false,
            cssClass: 'squares-target',
            squaresCssClass: 'squares-tiles',
            clickedTileCssClass: 'squares-current'
        }, options),

            that = this,
            i;

        if (!settings.$target) {
            throw "$target is required";
        }
        if (!settings.imageRatio) {
            throw "imageRatio is required";
        }

        settings.$target.addClass(settings.cssClass);

        this.backgroundAsImage = $('img', settings.$target);

        this.settings = settings;
        this.animationHelper = new TiledAnimationHelper(settings.delay);
        this.animationHelper.promise(this);

        // handle animation events
        this.progress(function (event, data) {

            if (event === prepareEvent) {
                that.animating = true;
                // show the tiles' container
                that.$squares.show();
                // force redraw
                document.body.style.display = 'none';
                document.body.style.display = 'block';

            } else if (event === endEvent) {

                that.setBackground().done(function () {
                    for (i = 0; i < that.tiles.length; i += 1) {
                        that.tiles[i].$element.hide();
                    }
                    that.$squares.hide();
                    setTimeout(function () {
                        that.animating = false;
                    }, 10);
                });
            }
        });

        this.addCallback(settings.prepare, prepareEvent);
        this.addCallback(settings.start, startEvent);
        this.addCallback(settings.step, stepEvent);
        this.addCallback(settings.end, endEvent);

        this.setGrid(settings);
    };
    TiledBackground.prototype.setNextBackground = function (background) {
        var that = this,
            i,
            $img = $('<img>'),
            loadDeferred = new $.Deferred(),
            setDeferred = new $.Deferred(),
            deferred = new $.Deferred();


        if (this.tiles && this.tiles.length > 0) {

            $.when(loadDeferred, setDeferred).done(function () {
                deferred.resolve();
            });

            $img.on('load', function () {
                loadDeferred.resolve();
            }).attr('src', background);


            for (i = 0; i < that.tiles.length; i += 1) {
                that.tiles[i].setBackground(background);
            }
            setDeferred.resolve();
        }
        return deferred.promise();
    };
    TiledBackground.prototype.setBackground = function () {
        var deferred = new $.Deferred();
        if (this.backgroundAsImage.length > 0) {

            // src attribute will not change
            if (this.backgroundAsImage.attr('src') === this.image) {
                deferred.resolve();
                return deferred.promise();
            }

            this.backgroundAsImage.off().on('load', function () {
                deferred.resolve();
            });
            this.backgroundAsImage.attr('src', this.image);

        } else {
            this.settings.$target.css('background-image',
                                      'url(' + this.image + ')');
            deferred.resolve();
        }
        return deferred.promise();
    };
    TiledBackground.prototype.updateBackgroundPosition = function () {
        if (this.backgroundAsImage.length > 0) {

            this.backgroundAsImage.offset(this.imageOffset);
            this.backgroundAsImage.height(this.imageSize.height);
            this.backgroundAsImage.width(this.imageSize.width);
        }
    };
    TiledBackground.prototype.update = function (settings) {

        if (settings) {
            if (settings.size) {
                this.settings.size = settings.size;
            }
            if (typeof settings.isStatic === 'boolean') {
                this.settings.isStatic = settings.isStatic;
            }
        }
        this.animating = false;
        this.setGrid(this.settings);
    };
    TiledBackground.prototype.setGrid = function (settings) {

        var that = this,
            width = settings.$target.width(),
            height = settings.$target.height(),
            ratio = width / height,

            imageSize =
                ratio > this.settings.imageRatio ? {
                    width: width,
                    height: width / this.settings.imageRatio
                } : {
                    width: this.settings.imageRatio * height,
                    height: height
                },

            imageOffset = {
                left: (width - imageSize.width) / 2,
                top: (height - imageSize.height) / 2
            },

            // get rows and columns
            nRows = Math.ceil(height / settings.tileSize),
            nColumns = Math.ceil(width / settings.tileSize),
            tiles = [],
            columns = [],

            // create tiles' container
            $squares = $('<div class="' + settings.squaresCssClass + '"></div>'),

            i,
            column,
            row,
            tile;


        if (height === 0) {
            console.warn('The height of target is 0: no tiles created');
        }

        // create and append the tiles
        for (i = 0; i < nRows * nColumns; i += 1) {
            column = (i % nColumns);
            row = Math.floor(i / nColumns);
            tile = new Tile({
                row: row,
                column: column,
                size: settings.tileSize,
                imgHeight: imageSize.height,
                imgWidth: imageSize.width,
                offset: imageOffset,
                isStatic: this.settings.isStatic
            });

            tiles.push(tile);
            if (!columns[column]) {
                columns[column] = [];
            }
            columns[column].push(tile);

            $squares.append(tile.$element);
        }

        that.tiles = tiles;

        that.animationHelper.setTiles(tiles, columns);

        // add the div containing the tiles to DOM
        if (that.$squares) {
            that.$squares.replaceWith($squares);
        } else {
            settings.$target.append($squares);
        }

        // set object properties
        this.columns = columns;
        this.nColumns = nColumns;
        this.nRows = nRows;
        this.$squares = $squares;
        this.imageSize = imageSize;
        this.imageOffset = imageOffset;
        this.updateBackgroundPosition();
    };
    TiledBackground.prototype.animate = function (options) {

        var that = this,

            settings = $.extend({
                x: null,            // required
                y: null,            // required
                image: null,        // required
                animation: 'columns',
                delay: this.settings.delay
            }, options);

        if (!settings.x || !settings.y || !settings.image) {
            throw "missing required properties";
        }


        if (!this.animating) {
            this.image = settings.image;
            this.setNextBackground(this.image).done(function () {

                that.animationHelper.animate(settings.x, settings.y,
                                             settings.animation, settings.delay);
            });
        }
    };
    TiledBackground.prototype.addCallback = function (callback, event) {
        if (callback) {
            this.progress(function (e) {
                if (e === event) {
                    callback.apply(this, Array.prototype.slice.call(arguments, 1));
                }
            });
        }
    };


    // jQuery plugin

    methods = {
        init: function (options) {
            options.$target = this;
            var tiled = this.tiledBackground || $(this).data('tiledBackground');
            if (!tiled) {
                tiled = new TiledBackground(options);
                this.tiledBackground = tiled;
                $(this).data('tiledBackground', tiled);
            }
            return this;
        },
        animate: function (options) {
            this.tiledBackground.animate(options);
            return this;
        },
        update: function (settings) {
            this.tiledBackground.update(settings);
            return this;
        },
//        prepare: function (callback) {
//            this.tiledBackground.addCallback(callback, prepareEvent);
//            return this;
//        },
//        start: function (callback) {
//            this.tiledBackground.addCallback(callback, startEvent);
//            return this;
//        },
//        step: function (callback) {
//            this.tiledBackground.addCallback(callback, stepEvent);
//            return this;
//        },
        end: function (callback) {
            this.tiledBackground.addCallback(callback, endEvent);
            return this;
        }
    };

    $.fn.squares = function (method) {

        if (!this.tiledBackground) {
            this.tiledBackground = $(this).data('tiledBackground');
        }

         // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this,
                Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist on jQuery.squares');
        }
    };

    window.TiledBackground = TiledBackground;

}(jQuery, window.Modernizr,
    typeof console === "undefined" || typeof console.log === "undefined" ?
            {
                log: function () {"use strict"; },
                warn: function () {"use strict"; }
            } : console));

