/*global window, setTimeout, clearTimeout, setInterval, clearInterval, alert, console,
jQuery, test, asyncTest, start, module, expect, ok, equal, notEqual,
TiledBackground*/
(function ($, console) {

    "use strict";
    
    $(function () {

        var $backgrounds = $('#target'),
            delay = 20,
            height = $(window).height(),
            width = $(window).width(),
            rows = Math.ceil(height / 150),
            columns = Math.ceil(width / 150),
            timer = 0,
            interval,
            nAnimations,
            counter = 0,
            tiled,
            imageRatio = 2.28245363766,

            index = 0,
            images = [
                '../images/img_main.jpg',
                '../images/img_fun_01.jpg',
                '../images/img_fun_03.jpg',
                '../images/img_fun_04.jpg'
            ];
        
        $('#width').html(width);
        $('#height').html(height);
        $('#rows').html(rows);
        $('#columns').html(columns);

        $backgrounds.css('height', height);

        // fire animation on click
        $backgrounds.on('click', function (e) {
            $backgrounds.squares('animate', {
                x: e.pageX,
                y: e.pageY,
                image: images[index]
            });
            index = (index + 1) % images.length;
        });

        function randomNumber(width, height) {
            return {
                x: Math.floor(Math.random() * width),
                y: Math.floor(Math.random() * height)
            };
        }

        test('random', function () {
            var random = randomNumber(width, height);
            ok(random.x <= width && random.x >= 0);
            ok(random.y <= height && random.y >= 0);
        });



        function runAnimation(maxTimeout) {
            var random = randomNumber(width, height),
                event = $.Event("click"),
                time,
                t;
            event.originalEvent = $.Event('mouseEvent');
            event.pageX = random.x;
            event.pageY = random.y;
            $backgrounds.trigger(event);
            if (maxTimeout) {
                time = Math.floor(Math.random() * maxTimeout);
                t = setTimeout(function () {
                    if (counter < nAnimations) {
                        runAnimation(maxTimeout);
                    } else {
                        clearTimeout(t);
                    }
                }, time);
            }
        }

        function startTimer() {
            timer = 0;
            if (interval) {
                clearInterval(interval);
            }
            interval = setInterval(function () {
                timer += 1;
                if (timer > 2000) {
                    clearInterval(interval);
                    ok(false, "animation should be finished");
                }
            }, 1);
        }

        function stopTimer() {
            console.log('duration: ' + timer);
            clearInterval(interval);
            ok(timer < 2000, "animation should be last less than 2s");
            if (counter >= nAnimations) {
                start();
            }
        }
        
        module('bare test', {
            setup: function () {
                tiled = new TiledBackground({
                    $target: $('#target'),
                    imageRatio: imageRatio
                });
            },
            teardown: function () {
                //$('.squares-tiles').remove();
            }
        });
        test("basic properties", function () {
            expect(0);
            var date = new Date();
            tiled.animate({
                x: 400,
                y: 400,
                image: images[index],
                animation: 'test'
            });
            tiled.progress(function (event) {
                if (event === 0) {
                    console.log('prepare: ' + (new Date() - date));
                } else if (event === 1) {
                    console.log('start: ' + (new Date() - date));
                } else if (event === 'startStep') {
                    console.log('startStep: ' + (new Date() - date));
                } else if (event === 2) {
                    console.log('step: ' + (new Date() - date));
                } else if (event === 3) {
                    console.log('end: ' + (new Date() - date));
                }
            });
        });
        test("animation next", function () {
            expect(1);
            var next = tiled.animationHelper.snakeAnimation.next,
                tile,
                nextTile;
            
            tile = tiled.tiles[0];
            
            equal(next(tile, false, false, 5, 7), {row: 1, column: 0});
        });

//        module('jquery plugin', {
//            setup: function () {
//                function sleep(millis) {
//                    var date = new Date(),
//                        curDate = null;
//                    do {
//                        curDate = new Date();
//                    } while (curDate - date < millis);
//                }
//                counter = 0;
//                $backgrounds.squares({
//                    imageRatio: imageRatio,
//                    tileSize: 150,
//                    animation: 'columns',
//                    delay: delay,
//                    start: function () {
//                        counter += 1;
//                        startTimer();
//                    },
//                    end: function () {
//                        stopTimer();
//                        clearInterval(interval);
//                    }
//                });
//            },
//            teardown: function () {
//                clearInterval(interval);
//            }
//        });
//
//        test("basic properties", function () {
//            notEqual($backgrounds.tiledBackground, null, "We expect value not to be null");
//        });
//        asyncTest("ss", function () {
//            nAnimations = 1;
//            $backgrounds.squares('end', function () {console.log(this); });
//            expect(nAnimations);
//            runAnimation();
//        });
//        asyncTest("animate with max timeout 1000", function () {
//            nAnimations = 10;
//            expect(nAnimations);
//            runAnimation(1000);
//        });
//        asyncTest("animate with max timeout 500", function () {
//            nAnimations = 10;
//            expect(nAnimations);
//            runAnimation(500);
//        });

    });
}(jQuery, typeof console === "undefined" || typeof console.log === "undefined" ?
            { log: jQuery.noop(), warn: jQuery.noop() } : console));

