/*global jQuery, window, console, alert, setInterval, clearInterval, document*/

(function ($, console) {

    "use strict";

    $(function () {

        var $backgrounds = $('#target'),
            $controls = $('#controls'),
            $backgroundsImg = $('img', $backgrounds),
            imageRatio = 2.28245363766,
            animation = $('.active', $controls).attr('id'),
            $delay = $('#delay_val', $controls),
            delay = parseInt($delay.val(), 10),
            $static = $('#static', $controls),
            isStatic = $static.is(':checked'),

            getDimensions = function () {
                var height = $(window).height();

                $backgrounds.height(height);
            },

            update = function () {
                getDimensions();
                $backgrounds.squares('update');
            },

            index = 0,
            images = [
                'images/img_main.jpg',
                'images/img_fun_01.jpg',
                'images/img_fun_03.jpg',
                'images/img_fun_04.jpg'
            ];

        // init
        getDimensions();
        $backgrounds.squares({
            imageRatio: imageRatio, // required (width / height)
            tileSize: 150,
            delay: delay,
            end: function () {
                console.log('ending animation');
            }
        });

        $backgrounds.squares('end', function () {
            console.log('another callback');
        });
        
        function testPerformance(duration) {
            var time = new Date(),
                counter = 0,
                interval = setInterval(function () {
                    var newDiv;
                    newDiv = $('<div></div>');
                    $(document.body).append(newDiv);
                    newDiv.hide().show();
                    newDiv.remove();
                    if (new Date() - time > duration) {
                        alert(counter);
                        clearInterval(interval);
                    }
                    counter += 1;
                }, 1);
        }

        // fire animation on click
        $('button:not(#test)', $controls).on('click', function (event) {
            animation = event.target.id;
            if (animation === 'custom') {
                $static.attr('checked', true);
                isStatic = true;
                $backgrounds.squares('update', { isStatic: isStatic });
                
                animation = function (tiles, row, column, delay, end) {
                    
                    var tempo = 0,
                        counter = 0,
                        i,
                        onAnimationEnd = function () {
                            counter += 1;
                            if (counter === tiles.length) {
                                end();
                            }
                        };
                    
                    for (i = 0; i < tiles.length; i += 1) {
                        tiles[i].$element.show();
                    }
                    for (i = 0; i < tiles.length; i += 1) {
                        tempo += delay;
                        tiles[i].$element.delay(tempo).animate({
                            height: tiles[i].size,
                            width: tiles[i].size
                        }, 'slow', onAnimationEnd);
                    }
                    //end();
                };
            }
            $(event.target).addClass('active').siblings().removeClass('active');
        });
        $delay.change(function () {
            delay = parseInt($delay.val(), 10);
        });
        $static.change(function () {
            isStatic = $static.is(':checked');
            $backgrounds.squares('update', { isStatic: isStatic });
        });
        $backgrounds.on('click', function (e) {
            $backgrounds.squares('animate', {
                x: e.pageX,
                y: e.pageY,
                image: images[index],
                animation: animation,
                delay: delay,
                isStatic: isStatic
            });
            index = (index + 1) % images.length;
        });
        $('#test', $controls).on('click', function () {
            testPerformance(1000);
        });

        update();
        window.onresize = update;
        
    });
}(jQuery, typeof console === "undefined" || typeof console.log === "undefined" ?
        {
            log: function () { "use strict"; },
            warn: function () { "use strict"; }
        } : console));
